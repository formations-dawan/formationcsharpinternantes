﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.DesignPatterns.Comportement.Observer.Exemple1
{
    internal class Client : IObservateur
    {
        public int? Id { get; set; }

        public string Nom { get; set; }

        public string Email { get; set; }

        public Client(string nom, string email)
        {
            Nom = nom;
            Email = email;
        }

        public void MettreAJour(ISujet sujet)
        {
            var p = sujet as Produit; // (Produit)sujet;

            var corpsMail = "Bonjour,\n le prix du produit a été modifié : " + p.Prix;

            /*var m = new MailMessage("noreply@dawan.fr",
                Email,
                "Changement de prix du " + p.Description,
                corpsMail);

            var client = new SmtpClient("smtp.free.fr");
            client.Credentials = CredentialCache.DefaultNetworkCredentials;
            client.Send(m);*/

            Debug.WriteLine(corpsMail);
        }
    }
}
