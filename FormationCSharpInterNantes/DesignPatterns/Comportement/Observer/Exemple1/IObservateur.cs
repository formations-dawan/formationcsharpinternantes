﻿namespace FormationCSharpInterNantes.DesignPatterns.Comportement.Observer.Exemple1
{
    internal interface IObservateur
    {
        void MettreAJour(ISujet sujet);
    }
}