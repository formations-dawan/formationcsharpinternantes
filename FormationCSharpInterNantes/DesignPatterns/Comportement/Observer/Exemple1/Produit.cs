﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.DesignPatterns.Comportement.Observer.Exemple1
{
    internal class Produit : ISujet
    {
        public int? Id { get; set; }
        public string Description { get; set; }

        private double _prix;

        public Produit()
        {
            Observateurs = new();
        }

        public double Prix
        {
            get => _prix;
            set
            {
                _prix = value;
                Notifier(this);
            } 
        }

        public List<IObservateur> Observateurs { get; set; }

        public void Attacher(IObservateur observateur)
        {
            if (!Observateurs.Contains(observateur))
                Observateurs.Add(observateur);
        }

        public void Detacher(IObservateur observateur)
        {
            Observateurs.Remove(observateur);
        }

        public void Notifier(ISujet sujet)
        {
            foreach (var obs in Observateurs)
                obs.MettreAJour(sujet);
        }
    }
}
