﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.DesignPatterns.Comportement.Observer.Exemple2
{
    internal class Desabonnement : IDisposable
    {
        private List<IObserver<EvenementChangementPrix>> _observers;
        private IObserver<EvenementChangementPrix> _observer;

        public Desabonnement(List<IObserver<EvenementChangementPrix>> observers, IObserver<EvenementChangementPrix> observer)
        {
            _observers = observers;
            _observer = observer;
        }

        public void Dispose()
        {
            if (_observers != null)
                _observers.Remove(_observer);
        }
    }
}
