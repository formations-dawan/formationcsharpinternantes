﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.DesignPatterns.Comportement.Observer.Exemple2
{
    internal class Article : IObservable<EvenementChangementPrix>
    {
        public string Description { get; set; }

        private List<IObserver<EvenementChangementPrix>> _observers;

        private double _prix;

        public Article()
        {
            _observers = new();
        }

        public double Prix
        {
            get => _prix;
            set
            {
                _prix = value;
                var e = new EvenementChangementPrix(DateTime.Now, "Changement prix de " + Description + " : " + Prix);

                foreach (var obs in _observers)
                    obs.OnNext(e);
            }
        }

        public IDisposable Subscribe(IObserver<EvenementChangementPrix> observer)
        {
            if (!_observers.Contains(observer))
                _observers.Add(observer);

            return new Desabonnement(_observers, observer);
        }
    }
}
