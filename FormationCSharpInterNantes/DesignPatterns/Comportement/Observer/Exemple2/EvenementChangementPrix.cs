﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.DesignPatterns.Comportement.Observer.Exemple2
{
    internal class EvenementChangementPrix
    {
        public DateTime Date { get; set; }
        public string MessageNotif { get; set; }

        public EvenementChangementPrix(DateTime date, string messageNotif)
        {
            Date = date;
            MessageNotif = messageNotif;
        }
    }
}
