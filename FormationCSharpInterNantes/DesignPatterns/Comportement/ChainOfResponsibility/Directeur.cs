﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FormationCSharpInterNantes.DesignPatterns.Comportement.ChainOfResponsibility.Plainte;
using static System.Console;

namespace FormationCSharpInterNantes.DesignPatterns.Comportement.ChainOfResponsibility
{
    internal class Directeur : MembreEquipe
    {
        public Directeur(string nom, MembreEquipe successeur) : base(nom, successeur)
        {
        }

        public override void Handle(Plainte requete)
        {
            WriteLine("Traitement par le directeur.");
            requete.Etat = EtatPlainte.Ferme;
        }
    }
}
