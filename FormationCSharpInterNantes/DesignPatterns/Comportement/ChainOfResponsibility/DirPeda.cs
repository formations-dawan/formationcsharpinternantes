﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FormationCSharpInterNantes.DesignPatterns.Comportement.ChainOfResponsibility.Plainte;
using static System.Console;

namespace FormationCSharpInterNantes.DesignPatterns.Comportement.ChainOfResponsibility
{
    internal class DirPeda : MembreEquipe
    {
        public DirPeda(string nom, MembreEquipe successeur) : base(nom, successeur)
        {
        }

        public override void Handle(Plainte requete)
        {
            if (requete.Type == 2)
            {
                WriteLine("Traitement par le directeur pédagogique");
                requete.Etat = EtatPlainte.Ferme;
            }
            else if (_successeur != null)
            {
                WriteLine("Le directeur pédagogique a remonté la demande à son supérieur.");
                _successeur.Handle(requete);
            }
        }
    }
}
