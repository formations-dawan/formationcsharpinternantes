﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.DesignPatterns.Comportement.ChainOfResponsibility
{
    internal abstract class MembreEquipe //Handler (dans le schéma UML)
    {
        private string _nom;
        protected MembreEquipe _successeur; //pour avoir le membre suivant dans la responsabilité

        protected MembreEquipe(string nom, MembreEquipe successeur)
        {
            _nom = nom;
            _successeur = successeur;
        }

        public abstract void Handle(Plainte requete);
    }
}
