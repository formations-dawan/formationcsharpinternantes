﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FormationCSharpInterNantes.DesignPatterns.Comportement.ChainOfResponsibility.Plainte;
using static System.Console;

namespace FormationCSharpInterNantes.DesignPatterns.Comportement.ChainOfResponsibility
{
    internal class Formateur : MembreEquipe
    {
        public Formateur(string nom, MembreEquipe successeur) : base(nom, successeur)
        {
        }

        public override void Handle(Plainte requete)
        {

            if (requete.Type == 1)
            {
                WriteLine("Traitement par le formateur");
                requete.Etat = EtatPlainte.Ferme;
            }
            else if (_successeur != null)
            {
                WriteLine("Le formateur a remonté la demande à son supérieur.");
                _successeur.Handle(requete);
            }
        }
    }
}
