﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.DesignPatterns.Structure.Decorator
{
    internal abstract class VoitureDecorator : IVoiture
    {
        private IVoiture _voiture;

        protected VoitureDecorator(IVoiture voiture)
        {
            _voiture = voiture;
        }

        public virtual void Assembler()
        {
            _voiture.Assembler();
        }
    }
}
