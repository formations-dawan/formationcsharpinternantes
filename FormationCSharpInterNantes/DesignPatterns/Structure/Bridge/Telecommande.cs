﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.DesignPatterns.Structure.Bridge
{
    internal abstract class Telecommande
    {
        protected IAppareil _appareil;

        protected Telecommande(IAppareil appareil)
        {
            DefinirAppareil(appareil);
        }

        public void DefinirAppareil(IAppareil appareil)
        {
            _appareil = appareil;
        }

        public abstract void ChangerVolume(int v);
        public abstract void ChangerCanal(int c);
        public abstract void AllumerOuEteindre();
    }
}
