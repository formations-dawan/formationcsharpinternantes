﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.DesignPatterns.Structure.Bridge
{
    internal interface IAppareil
    {
        bool EstAllume();
        void ChangerVolume(int v);
        void ChangerCanal(int c);
        void Allumer();
        void Eteindre();
    }
}
