﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.DesignPatterns.Structure.Bridge
{
    internal class Supertelecommande : TelecommandeBasique
    {
        public Supertelecommande(IAppareil appareil) : base(appareil)
        {
        }

        public void Sourdine()
        {
            ChangerVolume(0);
        }
    }
}
