﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.DesignPatterns.Structure.Bridge
{
    internal class TelecommandeBasique : Telecommande
    {
        public TelecommandeBasique(IAppareil appareil) : base(appareil)
        {
        }

        public override void AllumerOuEteindre()
        {
            if (_appareil.EstAllume())
            {
                _appareil.Eteindre();
                return;
            }

            _appareil.Allumer();
        }

        public override void ChangerCanal(int c)
        {
            _appareil.ChangerCanal(c);
        }

        public override void ChangerVolume(int v)
        {
            _appareil.ChangerVolume(v);
        }
    }
}
