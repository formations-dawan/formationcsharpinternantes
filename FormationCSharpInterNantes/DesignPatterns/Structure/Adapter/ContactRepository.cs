﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FormationCSharpInterNantes.DesignPatterns.Structure.Adapter
{
    internal class ContactRepository : IContactRepository
    {
        List<Contact> IContactRepository.DepuisXml(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            XmlNodeList noeuds = doc.DocumentElement.SelectNodes("/Contacts/Contact");

            List<Contact> contacts = new List<Contact>();
            foreach (XmlNode noeud in noeuds)
            {
                var c = new Contact
                {
                    Id = Convert.ToInt32(noeud.Attributes["id"].Value),
                    Nom = noeud.ChildNodes[0].InnerText
                };

                contacts.Add(c);
            }

            return contacts;
        }

        string IContactRepository.RecupererContactsXml(string cheminFichier)
        {
            using var stream = new StreamReader(cheminFichier);

            var res = stream.ReadToEnd();

            return res;
        }
    }
}
