﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.DesignPatterns.Structure.Adapter
{
    internal class JsonAdapter : IJsonAdapter //target
    {
        private readonly IContactRepository _xmlRepo; //adaptee

        public JsonAdapter(IContactRepository xmlRepo)
        {
            _xmlRepo = xmlRepo;
        }

        string IJsonAdapter.RecupererContactsJson(string cheminFichier)
        {
            string xml = _xmlRepo.RecupererContactsXml(cheminFichier);

            //Transformation en JSON
            List<Contact> contacts = _xmlRepo.DepuisXml(xml);
            return JsonSerializer.Serialize(contacts);
        }
    }
}
