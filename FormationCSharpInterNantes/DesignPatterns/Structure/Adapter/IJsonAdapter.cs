﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.DesignPatterns.Structure.Adapter
{
    internal interface IJsonAdapter
    {
        string RecupererContactsJson(string cheminFichier);
    }
}
