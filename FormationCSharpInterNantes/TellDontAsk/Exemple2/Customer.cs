﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.TellDontAsk.Exemple2
{
    internal class Customer
    {
        public string CustomerAddress { get; set; }

        public long Id { get; set; }

        public List<Percel> Percels { get; set; }
    }
}
