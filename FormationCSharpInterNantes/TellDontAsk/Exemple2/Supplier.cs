﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.TellDontAsk.Exemple2
{
    internal class Supplier
    {
        List<Customer> Customers { get; set; }

        public void Deliver(long customerId)
        {
            var cust = Customers.Find(c => c.Id == customerId);

            foreach (var percel in cust.Percels)
            {
                Console.WriteLine("Delivering percel to " + cust.CustomerAddress);
                // traitement relatif à la livraison
            }
        }
    }
}
