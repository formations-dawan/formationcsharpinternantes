﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.TellDontAsk.Exemple2
{
    internal class PercelDeliveryService
    {
        public void DeliverPercel(long customerId)
        {
            //accès au repository...
            var supp = new Supplier();
            supp.Deliver(customerId);
        }
    }
}
