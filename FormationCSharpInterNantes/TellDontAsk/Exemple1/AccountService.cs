﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.TellDontAsk.Exemple1
{
    internal class AccountService : IAccountService
    {
        private readonly IAccountRepository _accountRepository;

        public AccountService(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        public void Withdraw(int accountId, double amount)
        {
            Account c = _accountRepository.FindById(accountId);
            c.Withdraw(amount);
            _accountRepository.Save(c);
        }
    }
}
