﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.Polymorphisme
{
    internal class Table : IPliable
    {
        // Implémentation implicite
        /*public void Deplier()
        {

        }*/

        // Implémentation explicite
        void IPliable.Deplier()
        {
            Console.WriteLine("Déplier...");
        }

        void IPliable.Plier()
        {
            Console.WriteLine("Plier...");
        }
    }
}
