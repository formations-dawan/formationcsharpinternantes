﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.Polymorphisme
{
    internal class PolyTest
    {
        // ad-hoc (ou niveau de fonction avec des GetType)
        public static void Buy(Object obj)
        {
            if (obj is IPliable pliable)
            {
                pliable.Plier();
            }
        }

        // sous-typage (héritage / implémentation)
        public static void Buy(IPliable pliable)
        {
            // pas besoin de tester le type à chaque fois
            pliable.Plier();       
        }

        // types paramétriques (généricité)
        public static void Buy<T>(T pliable) where T : IPliable
        {
            // pas besoin de tester le type à chaque fois
            pliable.Plier();
        }
    }
}
