﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.Genericite
{
    /// <summary>
    /// Interface générique pour l'accès aux données
    /// </summary>
    /// <typeparam name="TTable"></typeparam>
    /// <typeparam name="TCle"></typeparam>
    internal interface IDao<TTable, TCle> where TTable : TableBase<TCle>
    {
        List<TTable> RecupererTout();

        int Inserer(TTable obj);
    }
}
