﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.Genericite
{
    internal abstract class TableBase<TCle>
    {
        public TCle Id { get; set; }
    }
}
