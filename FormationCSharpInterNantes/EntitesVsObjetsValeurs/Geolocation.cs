﻿namespace FormationCSharpInterNantes.EntitesVsObjetsValeurs
{
    // ValueObject : Objet immutable (renseigné à la création), valeur ne bouge pas
    public class Geolocation : IComparable<Geolocation>
    {
        private double _longitude;
        private double _latitude;

        public Geolocation(double longitude, double latitude)
        {
            _longitude = longitude;
            _latitude = latitude;
        }

        public int CompareTo(Geolocation other)
        {
            // comparaison basée sur la distance 
            double rayonTerre = 6371; //en km

            double dLat = EnRadians(other._latitude - _latitude);
            double dLon = EnRadians(other._longitude - _longitude);

            double sindLat = Math.Asin(dLat / 2);
            double sindLon = Math.Asin(dLon / 2);

            double a = Math.Pow(sindLat, 2) + Math.Pow(sindLon, 2) *
                Math.Cos(EnRadians(_latitude)) * Math.Cos(EnRadians(other._latitude));

            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

            double dist = rayonTerre * c;

            return Convert.ToInt32(dist);

        }

        public override bool Equals(object? obj)
        {
            return obj is Geolocation geolocation &&
                   _longitude == geolocation._longitude &&
                   _latitude == geolocation._latitude;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string? ToString()
        {
            return $"{_longitude}, {_latitude}";
        }

        private static double EnRadians(double angle) => Math.PI / 180 * angle;
    }
}