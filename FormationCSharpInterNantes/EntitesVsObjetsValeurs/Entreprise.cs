﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.EntitesVsObjetsValeurs
{
    internal class Entreprise : IComparable<Entreprise>
    {
        public int? Id { get; set; }
        public string Nom { get; set; }
        public Geolocation Localisation { get; set; }

        public Entreprise(int? id, string nom, Geolocation localisation)
        {
            Id = id;
            Nom = nom;
            Localisation = localisation;
        }

        public int CompareTo(Entreprise? other)
        {
            return Nom.CompareTo(other?.Nom);
        }

        public int ComparerParLocalisation(Entreprise entreprise)
        {
            return Localisation.CompareTo(entreprise.Localisation);
        }

        public override bool Equals(object? obj)
        {
            return obj is Entreprise e &&
                Id == e.Id;
        }

        public override int GetHashCode()
        {
            return 2108858624 + Id.GetHashCode();
        }
    }
}
