﻿// See https://aka.ms/new-console-template for more information
using FormationCSharpInterNantes.Aggregation.Faible;
using FormationCSharpInterNantes.Aggregation.Forte;
using FormationCSharpInterNantes.DesignPatterns.Comportement.ChainOfResponsibility;
using FormationCSharpInterNantes.DesignPatterns.Comportement.Observer.Exemple2;
using FormationCSharpInterNantes.DesignPatterns.Structure.Adapter;
using FormationCSharpInterNantes.DesignPatterns.Structure.Bridge;
using FormationCSharpInterNantes.DesignPatterns.Structure.Decorator;
using FormationCSharpInterNantes.DesignPatterns.Structure.Proxy;
using FormationCSharpInterNantes.Encapsulation;
using FormationCSharpInterNantes.EntitesVsObjetsValeurs;
using FormationCSharpInterNantes.Genericite;
using System.Security.AccessControl;
using static FormationCSharpInterNantes.DesignPatterns.Comportement.ChainOfResponsibility.Plainte;
using ProduitObserver = FormationCSharpInterNantes.DesignPatterns.Comportement.Observer.Exemple1.Produit;
using ContactObserver = FormationCSharpInterNantes.DesignPatterns.Comportement.Observer.Exemple2.Contact;


Console.WriteLine("Hello, World!");

#region Genericite
IDao<Produit, int> produitDao = new ProduitDao();
#endregion

#region Expressions Lambda
Thread thread = new(() => //Action
{
    Console.WriteLine("Bonjour depuis le thread 1");
});
thread.Start();

var lp = new List<Produit>
{
    new Produit { Id = 1, Description = "RTX 4090", Prix = 2000 },
    new Produit { Id = 2, Description =  "Balai", Prix = 25}
};
lp.Sort((Produit p1, Produit p2) => //Fonction
{
    if (p1.Prix == p2.Prix) return 0;
    else if (p1.Prix < p2.Prix) return -1;
    else return 1;
});
lp.ForEach(p => Console.WriteLine(p.Description + " : " + p.Prix));

//     x1      x2    sortie
Func<double, double, double> pow2 = (x1, x2) => Math.Pow(x1, x2);
double resP = pow2(23, 3);
Console.WriteLine("resP = " + resP);


/*delegate bool Predicat(Produit p);

public bool Find(Predicat callback)
{
    var found = false;

    foreach (var produit in produits)
    {
        if (callback(produit))
        {
            found = true;
            break;
        }               
    }

    return found;
}

Find(p => p.Id == 5);*/
#endregion

#region Encapsulation
var r = new Rectangle(12, 36);
var res = r.Aire();
r.Redim(24, 50);
Console.WriteLine(res);
#endregion

#region EntitesVsObjetsValeurs
var e1 = new Entreprise(1, "Dawan", new Geolocation(2.3527, 48.8543));
var e2 = new Entreprise(2, "Jehann", new Geolocation(3.37, 48.8543));

int compParNom = e1.CompareTo(e2);
int compParLocalisation = e1.ComparerParLocalisation(e2);

List<Entreprise> lstE = new() { e2, e1 };
lstE.Sort();
lstE.ForEach(e => Console.WriteLine(e.Nom));
#endregion

#region Aggregation
//Forte
var v = new Voiture(new Moteur("diesel")); //Aggrégation forte car voiture ne peut exister sans moteur
v.Garer(new Parking()); //Association

var v2 = new Voiture("hydrogène"); //Composition

//Faible
var client = new Client
{
    Nom = "Doe",
    Prenom = "John",
    Adresse = new()
    {
        Num = 15,
        Rue = "Vincent Gache"
    }
};
#endregion

#region Design Patterns - Adapter
IJsonAdapter adapter = new JsonAdapter(new ContactRepository());
//string json = adapter.RecupererContactsJson("contacts.xml");
#endregion

#region Design Patterns - Decorator
IVoiture voiture = new VoitureBasique();
voiture.Assembler();
Console.WriteLine("---------");

voiture = new SportDecorator(voiture);
voiture.Assembler();
Console.WriteLine("---------");

voiture = new LuxeDecorator(voiture);
voiture.Assembler();
Console.WriteLine("---------");

//autre solution
IVoiture voitureLuxeSport = new SportDecorator(new LuxeDecorator(new VoitureBasique()));
voitureLuxeSport.Assembler();
#endregion

#region Design Patterns - Proxy
IMessage message = new ProxyMessage(new MessageUtilisateur("bonjour"));
Console.WriteLine(message.RecupererContenu());
#endregion

#region Design Patterns - Chain Of Responsibility
//la chaine d'objets qui va traiter notre plainte
MembreEquipe chaine = new Formateur("Florian", new DirPeda("Deny", new Directeur("Jérôme", null)));

Console.WriteLine("---REQ 1---");
chaine.Handle(new Plainte(123, 1, "req1", EtatPlainte.Ouvert));

Console.WriteLine("---REQ 2---");
chaine.Handle(new Plainte(124, 2, "req2", EtatPlainte.Ouvert));

Console.WriteLine("---REQ 3---");
chaine.Handle(new Plainte(123, 3, "req3", EtatPlainte.Ouvert));
#endregion

#region Design Patterns - Observer 1
var produit = new ProduitObserver { Description = "RTX 4090", Prix = 2500 };

produit.Attacher(new FormationCSharpInterNantes.DesignPatterns.Comportement.Observer.Exemple1.Client("Florian", "fhenot@dawan.fr"));
produit.Attacher(new FormationCSharpInterNantes.DesignPatterns.Comportement.Observer.Exemple1.Client("Deny", "dleclerc@dawan.fr"));

produit.Prix = 2000; //ce changement de prix déclenche 2 notifications par email

#endregion

#region Design Patterns - Observer 2
var article = new Article { Description = "RTX 4090", Prix = 2000 };

IDisposable disC1 = article.Subscribe(new ContactObserver { Nom = "Florian" });
IDisposable disC2 = article.Subscribe(new ContactObserver { Nom = "Deny" });

article.Prix = 1500; //le changement déclenche les notifications

disC1.Dispose(); //unsubscribe de C1

article.Prix = 1000;
#endregion

Console.ReadKey();