﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.Aggregation.Faible
{
    internal class Adresse
    {
        public int Num { get; set; }

        public string Rue { get; set; }
    }
}
