﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.Aggregation.Forte
{
    internal class Voiture
    {
        // Aggrégation forte : objet voiture ne peut exister sans moteur
        public Moteur Moteur { get; }

        public Voiture(Moteur moteur)
        {
            Moteur = moteur;
        }

        // Composition
        public Voiture(string motorisation)
        {
            Moteur = new Moteur(motorisation);
        }

        // Association : objet voiture utilise objet Parking de manière
        public void Garer(Parking parc)
        {      
            parc.Garer();
        }
    }
}
