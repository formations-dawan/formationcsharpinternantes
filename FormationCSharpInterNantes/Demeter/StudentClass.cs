﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.Demeter
{
    internal class StudentClass
    {
        public IList<Student> Students { get; internal set; }

        public int CountStudents() => Students.Count;
    }
}
