﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterNantes.Demeter
{
    internal class School
    {
        private List<Grade> _grades;

        public int CountStudents()
        {
            int count = 0;

            foreach (var grade in _grades)
                count += grade.CountStudents();

            return count;
        }
    }
}
