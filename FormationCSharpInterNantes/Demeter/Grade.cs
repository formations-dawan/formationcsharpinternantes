﻿namespace FormationCSharpInterNantes.Demeter
{
    internal class Grade
    {
        public IList<StudentClass> Classes { get; internal set; }

        public int CountStudents()
        {
            int count = 0;

            foreach (var sc in Classes)
                count += sc.CountStudents();

            return count;
        }
    }
}
